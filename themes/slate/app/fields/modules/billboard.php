<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],

];

$billboard = new FieldsBuilder('billboard');

$billboard
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));


$billboard
	
	->addTab('content', ['placement' => 'left'])
		->addGroup('billboard')

			// Header
			->addText('header', [
					'label' => 'Header',
					'ui' => $config->ui,
				])
				->setInstructions('Header for the billboard')

			//Text		
			->addTrueFalse('paragraph_check', [
				'label' => 'Add Paragraph?'
				])
			->addWysiwyg('paragraph', [
				'label' => 'WYSIWYG',
				'ui' => $config->ui
			])
			->conditional('paragraph_check', '==', 1)

			//Image 
			->addGroup('billboard_image')

				//Large Image 
				->addImage('large')
					->setInstructions('Image background for billboard medium breakpoint and up')

				//Small Image 
				->addImage('small')
					->setInstructions('Image background for billboard small breakpoint')
				
			->endGroup()
			
			//Button
			->addFields(get_field_partial('modules.button'))
			
	   ->endGroup();

return $billboard;