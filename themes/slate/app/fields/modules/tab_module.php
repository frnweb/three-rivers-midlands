<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$tabs = new FieldsBuilder('tab_module');

$tabs
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));

$tabs
	->addTab('content', ['placement' => 'left'])

		//Header
		->addText('header', [
			'label' => 'Tab Header'
	    	])
	    	->setInstructions('This is optional')

	    //Link		
		->addLink('link_check', [
			'label' => 'Add link next to header?'
			])

		//Repeater
		->addRepeater('tabs', [
		  'min' => 1,
		  'max' => 10,
		  'button_label' => 'Add Card',
		  'layout' => 'block',
		  'wrapper' => [
	          'class' => 'deck',
	        ],
		])

		//Tab Title
		->addText('title', [
			'label' => 'Tab Title'
	    	])

		// Card
	  	->addFields(get_field_partial('modules.card'))

	  	//Image 
		->addGroup('tab_image', [
			'label' => 'Card Image'
		])
			->addTrueFalse('add_image', [
				'label' => 'Add Image to Card',
				'wrapper' => ['width' => 15]
				])
				->addImage('image', [
					'wrapper' => ['width' => 85]
				])
				->conditional('add_image', '==', 1)
		->endGroup();
    
return $tabs;